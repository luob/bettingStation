//
//  ChangePasswordViewController.m
//  BettingStation
//
//  Created by lin b on 15/8/30.
//  Copyright (c) 2015年 RunTeng. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "BettingStation-Prefix.pch"

static NSString *cellIdenti = @"ChangePasswordTableViewCellIdenti";
@interface ChangePasswordViewController ()

@property (nonatomic,strong) UITextField * oldTextField;
@property (nonatomic,strong) UITextField * secondTextField;

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    UINib *nib = [UINib nibWithNibName:@"ChangePasswordTableViewCell" bundle:nil];
//    [self.mainTableView registerNib:nib forCellReuseIdentifier:cellIdenti];
    [self setView];
    
}

- (void) setView
{
    self.view.backgroundColor = [UIColor getColor:@"f7f7f7"];
    UIView *upView = [[UIView alloc] initWithFrame:CGRectMake(0, 64+10, UIScreenWidth, 44)];
    upView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:upView];
    self.oldTextField = [[UITextField alloc] initWithFrame:CGRectMake(11, 5, UIScreenWidth-50, 35)];
    self.oldTextField.delegate = self;
    [self.oldTextField setFont:[UIFont systemFontOfSize:16]];

    self.oldTextField.placeholder = @"当前登录密码";
    [upView addSubview:self.oldTextField];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(UIScreenWidth-33, 16, 18, 12)];
    imageView.image = [UIImage imageNamed:@"Password"];
    [upView addSubview:imageView];
    
    UIView *downView = [[UIView alloc] initWithFrame:CGRectMake(0, 64+64, UIScreenWidth, 44)];
    downView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:downView];
    self.secondTextField = [[UITextField alloc] initWithFrame:CGRectMake(11, 5, UIScreenWidth-50, 35)];
    self.secondTextField.placeholder = @"新登录密码";
    [self.secondTextField setFont:[UIFont systemFontOfSize:16]];
    self.secondTextField.delegate = self;
    [downView addSubview:self.secondTextField];
    
    UIImageView *downImageView = [[UIImageView alloc] initWithFrame:CGRectMake(UIScreenWidth-33, 16, 18, 12)];
    downImageView.image = [UIImage imageNamed:@"Password"];
    [downView addSubview:downImageView];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(10, 64+135, UIScreenWidth-20, 42);
    [sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    [sureBtn setBackgroundImage:[UIImage imageNamed:@"login"] forState:UIControlStateNormal];
    [sureBtn addTarget:self action:@selector(sureClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:sureBtn];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick)];
    [self.view addGestureRecognizer:tap];
}

- (void)tapClick
{
    [self.oldTextField resignFirstResponder];
    [self.secondTextField resignFirstResponder];
}

- (void)sureClick
{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
