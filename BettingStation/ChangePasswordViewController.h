//
//  ChangePasswordViewController.h
//  BettingStation
//
//  Created by lin b on 15/8/30.
//  Copyright (c) 2015年 RunTeng. All rights reserved.
//

#import "BaseBackBtnViewController.h"

@interface ChangePasswordViewController : BaseBackBtnViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;

@end
