//
//  LoginViewController.h
//  BettingStation
//
//  Created by lin b on 15/8/28.
//  Copyright (c) 2015年 RunTeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *inputView;
@property (weak, nonatomic) IBOutlet UITextField *numberTextField;
@property (weak, nonatomic) IBOutlet UITextField *passWorldTextField;
@property (weak, nonatomic) IBOutlet UIButton *forgetPassWordBtn;
@property (weak, nonatomic) IBOutlet UIButton *swithBtn;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *joinBtn;
- (IBAction)switchClick:(id)sender;

- (IBAction)forgetPassWordClick:(id)sender;
- (IBAction)loginClick:(id)sender;
- (IBAction)applyClick:(id)sender;

@end
