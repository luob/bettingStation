
//
//  SettingViewController.m
//  BettingStation
//
//  Created by lin b on 15/8/30.
//  Copyright (c) 2015年 RunTeng. All rights reserved.
//

#import "SettingViewController.h"
#import "BettingStation-Prefix.pch"
#import "SetTableViewCell.h"
#import "ChangePasswordViewController.h"
#import "SecurityViewController.h"
#import "ViewSiteViewController.h"

static NSString *cellIndentifier = @"SetTableViewCellIdenti";

@interface SettingViewController ()
@property (nonatomic,strong) NSUserDefaults *defults;

@end

@implementation SettingViewController

- (void)viewDidLoad {
    self.title = @"设置";
    [super viewDidLoad];
    
    UINib *cellNib = [UINib nibWithNibName:@"SetTableViewCell" bundle:nil];
    [self.mainTableView registerNib:cellNib forCellReuseIdentifier:cellIndentifier];
    
    self.defults = [NSUserDefaults standardUserDefaults];
    [self setView];
    
    
}


- (void)setView
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, 80)];
    view.backgroundColor = [UIColor whiteColor];
    
    self.mainTableView.tableHeaderView = view;
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 15, 52, 52)];
    imageView.backgroundColor = [UIColor greenColor];
    [view addSubview:imageView];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(12+62, 20, UIScreenWidth-74, 19)];
    [nameLabel setFont:[UIFont systemFontOfSize:17]];
    nameLabel.text = @"福建体育彩票222站";
    [view addSubview:nameLabel];
    
    UILabel *adressLabel = [[UILabel alloc] initWithFrame:CGRectMake(74, 50, UIScreenWidth-74, 13)];
    [adressLabel setFont:[UIFont systemFontOfSize:13]];
    adressLabel.textColor = [UIColor getColor:@"b3b3b3"];
    adressLabel.text = @"五一中路";
    [view addSubview:adressLabel];
    
    UIImageView *upLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWidth, .5)];
    upLine.backgroundColor = [UIColor getColor:@"dcdde2"];
    [view addSubview:upLine];
    
    UIImageView *downLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, 79.5, UIScreenWidth, .5)];
    downLine.backgroundColor = [UIColor getColor:@"dcdde2"];
    [view addSubview:downLine];
    
}

#pragma mark -  UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return 2;
            break;
        case 2:
            return 2;
            break;
            
        default:
            break;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (indexPath.section == 2 && indexPath.row == 0) {
        if ([self.defults objectForKey:@"isswith"]) {
            [cell.clickBtn setBackgroundImage:[UIImage imageNamed:@"swith_select"] forState:UIControlStateNormal];
        }else{
            [cell.clickBtn setBackgroundImage:[UIImage imageNamed:@"swith_unselect"] forState:UIControlStateNormal];
        }
    }else{
        [cell.clickBtn setBackgroundImage:[UIImage imageNamed:@"set_seacher"] forState:UIControlStateNormal];

    }
    
    switch (indexPath.section) {
        case 0:
            cell.iconImageView.image = [UIImage imageNamed:@"Site"];
            cell.nameLabel.text = @"站点资料";
            cell.downLine.hidden = YES;
            break;
        case 1:
            if (indexPath.row == 0) {
                cell.iconImageView.image = [UIImage imageNamed:@"change_password"];
                cell.nameLabel.text = @"修改密码";

            }else{
                cell.iconImageView.image = [UIImage imageNamed:@"protect"];
                cell.nameLabel.text = @"密保问题";

            }
            
            break;
        case 2:
            if (indexPath.row == 0) {
                cell.iconImageView.image = [UIImage imageNamed:@"ticketTip"];
                cell.nameLabel.text = @"出票提醒";
                cell.downLine.hidden = YES;

            }else{
                cell.iconImageView.image = [UIImage imageNamed:@"about"];
                cell.nameLabel.text = @"关于我们";
//                NSLayoutConstraint *con = [cell.upLine ]

            }
            
            break;
            
        default:
            break;
    }

    return cell;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

#pragma mark - UITableviewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 2) {
        return 25;
    }
    return 16;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 3) {
        return 63;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section != 2) return nil;
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, 63)];
    UIButton *logOutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logOutBtn.frame = CGRectMake(24, 26, 273, 37);
    [logOutBtn setTitle:@"安全退出" forState:UIControlStateNormal];
    [logOutBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [logOutBtn addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
    [logOutBtn setBackgroundImage: [UIImage imageNamed:@"logout"]  forState:UIControlStateNormal];
    [view addSubview:logOutBtn];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中后的反显颜色即刻消失
    switch (indexPath.section) {
        case 0:
        {
            ViewSiteViewController *viewSite = [[ViewSiteViewController alloc] init];
            [self.navigationController pushViewController:viewSite animated:YES];
        }
            
            break;
        case 1:
            if (indexPath.row == 0) {
                ChangePasswordViewController *change = [[ChangePasswordViewController alloc] init];
                [self.navigationController pushViewController:change animated:YES];
            }else{
                SecurityViewController *security = [[SecurityViewController alloc] init];
                [self.navigationController pushViewController:security animated:YES];
            }
            
            break;
        case 2:
            
            break;
            
        default:
            break;
    }

}


- (void)logout:(id)sender
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
