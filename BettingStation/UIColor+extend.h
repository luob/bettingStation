//
//  UIColor+extend.h
//  BettingStation
//
//  Created by lin b on 15/8/30.
//  Copyright (c) 2015年 RunTeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (extend)

/*
 将十六进制的颜色值转为objective-c的颜色
 */
+ (id)getColor:(NSString *) hexColor;

@end
