//
//  SecurityViewController.m
//  BettingStation
//
//  Created by lin b on 15/8/30.
//  Copyright (c) 2015年 RunTeng. All rights reserved.
//

#import "SecurityViewController.h"
#import "BettingStation-Prefix.pch"

@interface SecurityViewController ()<UITextFieldDelegate>

@property (nonatomic,strong) UITextField *anserTextField;

@end

@implementation SecurityViewController

- (void)viewDidLoad {
    self.title = @"密保问题";
    [super viewDidLoad];
    [self setUp];
}

- (void) setUp
{
    self.view.backgroundColor = [UIColor getColor:@"f7f7f7"];
    UIView *upView = [[UIView alloc] initWithFrame:CGRectMake(0, 64+10, UIScreenWidth, 88)];
    upView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:upView];
    
    UILabel *questionTipLabel = [[UILabel alloc] initWithFrame:CGRectMake(18, 15, 60, 15)];
    questionTipLabel.text = @"安全问题";
    [questionTipLabel setFont:[UIFont boldSystemFontOfSize:15]];
    questionTipLabel.textAlignment = NSTextAlignmentRight;
    [upView addSubview:questionTipLabel];

    UILabel *questionLabel = [[UILabel alloc] initWithFrame:CGRectMake(116, 14, UIScreenWidth-116, 15)];
    questionLabel.text = @"我爸爸的姓名是什么";
    [questionLabel setFont:[UIFont systemFontOfSize:15]];
    questionLabel.textAlignment = NSTextAlignmentLeft;
    [upView addSubview:questionLabel];
    
    
    UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(18, 44, UIScreenWidth-18, .5)];
    line.backgroundColor = [UIColor getColor:@"dedede"];
    [upView addSubview:line];
    
    UILabel *answerTipLabel = [[UILabel alloc] initWithFrame:CGRectMake(18, 44+15, 60, 15)];
    answerTipLabel.text = @"回答";
    [answerTipLabel setFont:[UIFont boldSystemFontOfSize:15]];
    answerTipLabel.textAlignment = NSTextAlignmentRight;
    [upView addSubview:answerTipLabel];
    
    self.anserTextField = [[UITextField alloc] initWithFrame:CGRectMake(116, 44+5, UIScreenWidth-116, 35)];
    self.anserTextField.placeholder = @"请输入回答内容";
    [self.anserTextField setFont:[UIFont systemFontOfSize:16]];
    self.anserTextField.textColor = [UIColor getColor:@"b5b5b5"];
    self.anserTextField.delegate = self;
    [upView addSubview:self.anserTextField];
    
  
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(10, 64+135, UIScreenWidth-20, 42);
    [sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    [sureBtn setBackgroundImage:[UIImage imageNamed:@"login"] forState:UIControlStateNormal];
    [sureBtn addTarget:self action:@selector(sureClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:sureBtn];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick)];
    [self.view addGestureRecognizer:tap];
}

- (void)sureClick
{
    
}

- (void)tapClick
{
    [self.anserTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
