//
//  BaseBackBtnViewController.h
//  BettingStation
//
//  Created by lin b on 15/8/29.
//  Copyright (c) 2015年 RunTeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseBackBtnViewController : UIViewController

- (void)click_popViewController;
- (void)createBackItem;

@end
