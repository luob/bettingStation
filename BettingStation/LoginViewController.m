//
//  LoginViewController.m
//  BettingStation
//
//  Created by lin b on 15/8/28.
//  Copyright (c) 2015年 RunTeng. All rights reserved.
//

#import "LoginViewController.h"
#import "GetBackViewController.h"
#import "SettingViewController.h"

static NSString *const isAutoLog  = @"isautolog";

@interface LoginViewController ()

@property (nonatomic,strong) NSUserDefaults *defults ;


@end

@implementation LoginViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [[UIApplication sharedApplication] setStatusBarHidden:YES];//显示状态栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];

}



- (void)viewDidDisappear:(BOOL)animated
{
    [super  viewDidDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUp];
}

- (void)setUp
{
    self.inputView.layer.cornerRadius = 5.0;
    self.defults = [NSUserDefaults standardUserDefaults];
    if ([[self.defults valueForKey:isAutoLog] isEqualToString:@"1"]) {
        [self.swithBtn setBackgroundImage:[UIImage imageNamed:@"swith_select"] forState:UIControlStateNormal];
    }else{
        [self.swithBtn setBackgroundImage:[UIImage imageNamed:@"swith_unselect"] forState:UIControlStateNormal];
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick)];
    [self.view addGestureRecognizer:tap];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"设置" forState:UIControlStateNormal];
    btn.frame = CGRectMake(100, 400, 100, 40);
    [self.view addSubview:btn];
    [btn addTarget:self action:@selector(toSet) forControlEvents:UIControlEventTouchUpInside];
}

- (void)toSet
{
    SettingViewController *set = [[SettingViewController alloc] init];
    [self.navigationController pushViewController:set animated:YES];
}

- (void)tapClick
{
    [self.numberTextField resignFirstResponder];
    [self.passWorldTextField resignFirstResponder];
    
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) ;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFiled delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = CGRectMake(0, -88, self.view.frame.size.width, self.view.frame.size.height) ;
    } completion:^(BOOL finished) {
        
    }];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}

// 是否可以自动登录

- (IBAction)switchClick:(id)sender {
    if ([[self.defults valueForKey:isAutoLog] isEqualToString:@"1"]) {
        [self.defults setValue:@"0"forKey:isAutoLog];
        [self.swithBtn setBackgroundImage:[UIImage imageNamed:@"swith_unselect"] forState:UIControlStateNormal];
    }else{
        [self.defults setValue:@"1"forKey:isAutoLog];
        [self.swithBtn setBackgroundImage:[UIImage imageNamed:@"swith_select"] forState:UIControlStateNormal];
    }
    NSLog(@"%@",[self.defults valueForKey:isAutoLog]);

}

- (IBAction)forgetPassWordClick:(id)sender {
    GetBackViewController *getBack = [[GetBackViewController alloc] init];
    [self.navigationController pushViewController:getBack animated:YES];
}

- (IBAction)loginClick:(id)sender {
}

- (IBAction)applyClick:(id)sender {
}
@end
