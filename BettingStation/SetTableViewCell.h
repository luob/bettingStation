//
//  SetTableViewCell.h
//  BettingStation
//
//  Created by lin b on 15/8/30.
//  Copyright (c) 2015年 RunTeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *clickBtn;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UIImageView *upLine;
@property (weak, nonatomic) IBOutlet UIImageView *downLine;

@end
