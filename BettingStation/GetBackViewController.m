//
//  GetBackViewController.m
//  BettingStation
//
//  Created by lin b on 15/8/29.
//  Copyright (c) 2015年 RunTeng. All rights reserved.
//

#define UIScreenWith self.view.frame.size.width
#import "GetBackViewController.h"

@interface GetBackViewController ()

@end

@implementation GetBackViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"忘记密码";

    [self setView];
}

- (void) setView
{
    
    UIView *upView = [[UIView alloc] initWithFrame:CGRectMake(0, 64+17, UIScreenWith, 88)];
    upView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:upView];
    
    
    UIImageView *upline = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, UIScreenWith, 0.5)];
    upline.backgroundColor = [UIColor colorWithRed:.87 green:.87 blue:.87 alpha:1];
    [upView addSubview:upline];
    
    
    UILabel *questionTipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 17, 75, 14)];
    questionTipLabel.text = @"安全问题";
    questionTipLabel.textAlignment = NSTextAlignmentRight;
    [questionTipLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [upView addSubview:questionTipLabel];
    
    
    UILabel *questionLabel = [[UILabel alloc] initWithFrame:CGRectMake(116, 16, UIScreenWith-116, 14)];
    questionLabel.text = @"我爸爸的名字叫什么";
    questionLabel.textColor = [UIColor blackColor];
    [questionLabel setFont:[UIFont systemFontOfSize:14]];
    [upView addSubview:questionLabel];

    
    UIImageView *midLine = [[UIImageView alloc] initWithFrame:CGRectMake(15, 44, UIScreenWith-15, 0.5)];
    midLine.backgroundColor = [UIColor colorWithRed:.87 green:.87 blue:.87 alpha:1];

    [upView addSubview:midLine];
    
    
    UILabel *answerTipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 44+17, 75, 14)];
    answerTipLabel.text = @"回答";
    answerTipLabel.textAlignment = NSTextAlignmentRight;
    [answerTipLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [upView addSubview:answerTipLabel];
    
    UITextField *answerTextFiled = [[UITextField alloc] initWithFrame:CGRectMake(116, 44+16, UIScreenWith-116, 14)];
    answerTextFiled.delegate = self;
    [answerTextFiled setFont:[UIFont systemFontOfSize:14]];
    answerTextFiled.placeholder = @"请输入回答内容";
    [upView addSubview:answerTextFiled];
    
    UIImageView *downLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, 87, UIScreenWith, 0.5)];
    downLine.backgroundColor = [UIColor colorWithRed:.87 green:.87 blue:.87 alpha:1];
    [upView addSubview:downLine];
    
    
    
    UIView *downView = [[UIView alloc] initWithFrame:CGRectMake(0, 64+116, UIScreenWith, 44)];
    downView.backgroundColor  = [UIColor whiteColor];
    [self.view addSubview:downView];
    
    
    UILabel *passWordTipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 16, 75, 14)];
    passWordTipLabel.text = @"新密码";
    passWordTipLabel.textAlignment = NSTextAlignmentRight;
    [passWordTipLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [downView addSubview:passWordTipLabel];
    
    UITextField *passwordTextFiled = [[UITextField alloc] initWithFrame:CGRectMake(116, 16, UIScreenWith-116, 14)];
    passwordTextFiled.delegate = self;
    [passwordTextFiled setFont:[UIFont systemFontOfSize:14]];
    passwordTextFiled.placeholder = @"请输入新密码";
    [downView addSubview:passwordTextFiled];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(UIScreenWith-36, 16, 21, 11)];
    imageView.image = [UIImage imageNamed:@"Password"];
    [downView addSubview:imageView];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(15, 64+116+44+33, UIScreenWith-30, 44);
    [sureBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:17]];
    [sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    [sureBtn setBackgroundImage:[UIImage imageNamed:@"login"] forState:UIControlStateNormal];
    [sureBtn setBackgroundImage:[UIImage imageNamed:@"login"] forState:UIControlStateHighlighted];

    [sureBtn addTarget:self action:@selector(sureClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:sureBtn];
}


- (void)sureClick
{
    
}

#pragma mark - UITextField
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
